# VPC Flowlogs To Splunk - (VFS)

This is a proof of concept on how to transfer logs to [Splunk](https://www.splunk.com) from [AWS CloudWatch](https://aws.amazon.com/cloudwatch/) via [AWS lambda](https://aws.amazon.com/lambda/). It also has a nice
of using [AWS DynamoDB](https://aws.amazon.com/dynamodb/) as a backup queue in case your Splunk endpoint becomes inaccessible for some reason.


## Run through of the Design

The included [AWS CloudFormation](https://aws.amazon.com/cloudformation/) Template is rather straightforward. It sets up 
a [VPC](https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Introduction.html) with two 
[EC2](https://aws.amazon.com/ec2/) Instances that ping each other infinitely to simulate network traffic. Also this gets
logged in the VPC FLowLogs in CloudWatch. The Template also sets up the other prerequisites to make this all work.

Depending on your particular situation, your Splunk setup may not be as available as CloudWatch - though it has better features. You may
even be in a situation where data loss is unacceptable. There needs to be some form of stopgap. I considered a few options, 
some overkill and some that would require additional ops overhead. I settled on DynamoDB because it's easy to setup and
most importantly, it's region wide. That means, if it's ever down, chances are the VPC would be down as well. It also
comes with a very generous perpetual free tier with 25 GB of storage. Each Flow log record is about 105 bytes. It's a no brainer. 

The crème de la crème of a Lambda function I wrote, has a simple architecture. There is a log forwarding interface that is
subclassed into two targets; Splunk and DynamoDB. There is also a Queue class that provides an iterator around the DynamoDB Table.
It is provided as an Iterator as there is no telling how big the Table will get, and the Lambda function has limited RAM.
When the Lambda handler is invoked, it first loops through the queue and appends it to the batch sent over from CloudWatch.
It will then try to batch the events then forward them to Splunk. If it fails to get through to Splunk, they are written to 
the DynamoDB Table. Simple :)


## Getting Started

These instructions will get you a copy of the project up in your AWS Account.

### Prerequisites

The following is adapted from [Splunk's Blog](https://www.splunk.com/blog/2017/02/03/how-to-easily-stream-aws-cloudwatch-logs-to-splunk.html)

1. This CloudFormation Template requires the destination Splunk Enterprise server to have enabled and configured the [Splunk HTTP Event Collector](http://dev.splunk.com/view/event-collector/SP-CAAAE6M).*
It is also IMPERATIVE that it be served over TLS/HTTPS.

2. Install Splunk Add-on for AWS. Note that since we’ll be using Splunk HEC, we will *not* be relying on any modular 
input from the Add-on to collect from CloudWatch Logs or VPC Flow Logs. However, we will leverage the data parsing 
logic (i.e. sourcetypes) that already exist in the Add-on to automatically parse the VPC Flow logs records and extract
the fields.

3. Create an HEC token from Splunk Enterprise. Refer to [Splunk HEC docs](http://docs.splunk.com/Documentation/Splunk/latest/Data/UsetheHTTPEventCollector#Configure_HTTP_Event_Collector_in_Splunk_Web) for detailed instructions.
When configuring the input settings, make sure to specify “aws:cloudwatchlogs:vpcflow” as sourcetype. This is important to enable automatic fields extractions. Make sure to take note of your new HEC token value.
Note: For Splunk Cloud deployments, HEC must be enabled by Splunk Support.
Here’s how the data input settings would look like:
 
![Configure-Splunk-HEC.png](https://www.splunk.com/content/dam/splunk-blogs/images/2017/02/1.-Configure-Splunk-HEC.png "Configure-Splunk-HEC.png")

### Installing

Provided that you have the above prerequisites setup, you can just click the button below and fill in the parameters you 
got from the HEC. Oh, you can switch to a region that is closer to you.

[![cloudformation-launch-button](https://s3.amazonaws.com/cloudformation-examples/cloudformation-launch-stack.png)](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=VFS&templateURL=https://s3.amazonaws.com/ithance-devops/VPC_Flowlogs_to_Splunk.yaml)

Give it a few minutes, then you should start seeing logs in Splunk. Just search for:

```
sourcetype="aws:cloudwatchlogs:vpcflow"

```
## Deployment

If you want to deploy the lambda function yourself, you need to do the following:

1. Use pip to install the Dependencies
2. Place the source Python files and the dependencies in the same root directory
3. Zip all the files into one archive
4. Upload the zip archive to either S3 or any other server so long as it is accessible via https
5. Update the Cloudformation Template to Use your new source for the lambda function
6. Deploy the CloudFormation Template - Yea!

## Built With

* [Splunk Handler](https://github.com/zach-taylor/splunk_handler) - Used to forward logs to Splunk
* [boto3](https://github.com/boto/boto3) - AWS SDK for Python

## Contributing

Feel free to contribute an issue or pull request:

1. Check for existing issues and PRs
2. Fork the repo, and clone it locally
3. Create a new branch for your contribution
4. Push to your fork and submit a pull request

## Versioning

Currently version 0.1.2 - *Use at your own Risk!!!*

## Authors

* **Marc Byfield** - *Initial work*

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the terms of the [MIT license](http://opensource.org/licenses/MIT).

## Acknowledgments

This was inspired by a __Nerd Out__ session I recently had with [Cal Leeming](https://www.linkedin.com/in/sleepycal/) - GREAT Guy!.
It was a fun little project, though I spent more debugging Splunk than anything else Lol.
