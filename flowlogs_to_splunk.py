import base64
import gzip
import json
import os

from log_forwarding import SplunkLogForwarder
from queue_consumer import DynamoDBQueueConsumer

SPLUNK_HEC_URL = os.environ['SPLUNK_HEC_URL']
SPLUNK_HEC_PORT = os.environ['SPLUNK_HEC_PORT']
SPLUNK_HEC_TOKEN = os.environ['SPLUNK_HEC_TOKEN']
EVENT_QUEUE_TABLE = os.environ['EVENT_QUEQUE_TABLE']
AWS_REGION = os.environ['AWS_REGION']


def handler(event, context):
    """
    Stream events from AWS CloudWatch Logs to Splunk.
    This function streams AWS CloudWatch Logs to Splunk using
    Splunk's HTTP event collector API.

    :param event: The Batch of CloudWatch Events
    :param context: Metadata about the Lambda function itself
    :return:
    """
    print(f'Received event: {json.dumps(event, sort_keys=True, indent=4)}')

    queue = DynamoDBQueueConsumer(region=AWS_REGION, table=EVENT_QUEUE_TABLE)

    # CloudWatch Logs data are base64 encoded so decode here
    payload = base64.b64decode(event['awslogs']['data'])

    # CloudWatch Logs are gzip compressed so expand here
    result = str(gzip.decompress(payload), encoding='ascii')

    parsed = json.loads(result, encoding='ascii')
    print(f'Decoded payload: {json.dumps(parsed, sort_keys=True, indent=4)}')
    log_events = parsed['logEvents']

    # If there are any items in the queue, just add them to this sessions batch and send them off
    print(f'{len(queue)} events in Queue')
    for event in queue:
        log_events.append(event)
    print(f'{len(log_events)} events to process')
    if log_events:
            splunk = SplunkLogForwarder(host=SPLUNK_HEC_URL, port=SPLUNK_HEC_PORT, token=SPLUNK_HEC_TOKEN,
                                        function_name=context.function_name)
            for event in log_events:
                # For some reason, some events can be empty
                if event:
                    if 'id' in event:
                        print(f'Sending event #: {event["id"]} to Splunk')
                    splunk.forward(event)
            splunk.finalize()
            print(f'Processed {len(log_events)} log events!')
    else:
        print("There were no log events to process")
