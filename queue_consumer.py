import boto3
from botocore.exceptions import ClientError


class DynamoDBQueueConsumer(object):
    """
    This Class acts as our Fallback Quque implemented as an Iterator and backed by DynamoDB
    """

    def __init__(self, region: str = 'us-east-1', table: str = 'VPCLogQueue', endpoint_url: str =None):
        self.dynamodb = boto3.resource('dynamodb', region_name=region, endpoint_url=endpoint_url)
        self.table = self.dynamodb.Table(table)
        self._resume_key = {}
        self.__len = 0
        self.__events = self.__load_batch()

    def __iter__(self):
        return self

    def __next__(self):
        # Queue may be completely empty, don't scan queue again
        if len(self) == 0:
            raise StopIteration
        else:
            if not self.__events:
                print("Getting next batch of events")
                self.__events = self.__load_batch()
            self.__remove_batch()
            self.__len -= 1
            return self.__events.pop()

    def __len__(self):
        return self.__len

    # Get next batch from the table
    def __load_batch(self):
        try:
            if not self._resume_key:
                response = self.table.scan(
                    Limit=100
                )
            else:
                response = self.table.scan(
                    Limit=100,
                    ExclusiveStartKey=self._resume_key
                )
        except ClientError as error:
            print(error.response['Error']['Message'])
            return []
        else:
            if not response['Items']:
                return []
            if 'LastEvaluatedKey' in response:
                self._resume_key = response['LastEvaluatedKey']
            self.__len = response['ScannedCount']
            return response['Items']

    # remove batch from the table so it wont be re-read
    def __remove_batch(self):
        print("Removing batch from queue")
        with self.table.batch_writer() as writer:
            for event in self.__events:
                writer.delete_item(Key={'timestamp': event['timestamp']})
