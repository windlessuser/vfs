import abc
import json
import logging
import traceback

import os

import re
from botocore.exceptions import ClientError
from splunk_handler import SplunkHandler
from splunk_handler import force_flush
import boto3

logger = logging.getLogger('Splunker')
logger.setLevel(level=logging.DEBUG)

EVENT_QUEUE_TABLE = os.environ['EVENT_QUEQUE_TABLE']
AWS_REGION = os.environ['AWS_REGION']


class LogForwarder(object, metaclass=abc.ABCMeta):
    """
    Interface for our Log Forwarders
    """

    @abc.abstractmethod
    # Accumulates the Events
    def forward(self, event):
        pass

    @abc.abstractmethod
    # Sends them off to the external store
    def finalize(self):
        pass

class _MySplunkHandler(SplunkHandler):
    """
    Had to hack the SplunkHandler to make sure it sends the correct timestamp.
    It also falls back to DynamoDB in case it can't reach Splunk
    """

    def __init__(self, host, port, token, index,
                 hostname=None, source=None, sourcetype='text',
                 verify=True, timeout=60, flush_interval=15.0,
                 queue_size=5000, debug=False, retry_count=5,
                 retry_backoff=2.0):

        self.dynamodb = DynamoDBLogForwarder(region=AWS_REGION, table=EVENT_QUEUE_TABLE)

        super().__init__(host, port, token, index,
                         hostname=hostname, source=source, sourcetype=sourcetype,
                         verify=verify, timeout=timeout, flush_interval=flush_interval,
                         queue_size=queue_size, debug=debug, retry_count=retry_count,
                         retry_backoff=retry_backoff)

        # Overrode this method so that it can handle the correct timestamp

    def format_record(self, record):
        self.write_debug_log("format_record() called")

        if self.source is None:
            source = record.pathname
        else:
            source = self.source

        import time
        current_time = time.time() if not (isinstance(record.msg, dict) and not (
                'timestamp' not in record.msg)) else int(record.msg["timestamp"])
        if self.testing:
            current_time = None

        params = {
            'time': current_time,
            'host': self.hostname,
            'index': self.index,
            'source': source,
            'sourcetype': self.sourcetype,
            'event': record.msg['message'] if isinstance(record.msg, dict) and 'message' in record.msg else self.format(
                record),
        }

        self.write_debug_log("Record dictionary created")

        import json
        formatted_record = json.dumps(params, sort_keys=True)
        self.write_debug_log("Record formatting complete")

        return formatted_record

    def _splunk_worker(self, payload=None):
        self.write_debug_log("_splunk_worker() called")

        if self.flush_interval > 0:
            # Stop the timer. Happens automatically if this is called
            # via the timer, does not if invoked by force_flush()
            self.timer.cancel()

            queue_is_empty = self.empty_queue()

        if not payload:
            payload = self.log_payload

        if payload:
            self.write_debug_log("Payload available for sending")
            url = 'https://%s:%s/services/collector' % (self.host, self.port)
            self.write_debug_log("Destination URL is " + url)

            try:
                self.write_debug_log("Sending payload: " + payload)
                r = self.session.post(
                    url,
                    data=payload,
                    headers={'Authorization': "Splunk %s" % self.token},
                    verify=self.verify,
                    timeout=self.timeout,
                )
                r.raise_for_status()  # Throws exception for 4xx/5xx status
                self.write_debug_log("Payload sent successfully")

            except Exception as e:
                try:
                    self.write_log("Exception in Splunk logging handler: %s" % str(e))
                    payload = re.sub(r'(})', r'\1,', payload)  # Insert a comma between each record
                    payload = payload.rsplit(",", 1)[0]  # remove last comma at the end
                    payload = f'[{payload}]'  # Turn payload into a JSON array
                    print(f'Sending Payload: {payload} to DynamoDB')
                    events = json.loads(payload)
                    for event in events:
                        self.dynamodb.forward(event)
                    self.dynamodb.finalize()
                except ClientError as error:
                    self.write_log(f'Failed to write to DynamoDB: {error.response["Error"]["Message"]}')
                except:
                    self.write_log(traceback.format_exc())

            self.log_payload = ""
        else:
            self.write_debug_log("Timer thread executed but no payload was available to send")


class SplunkLogForwarder(LogForwarder):
    """
    This Forwarder accumulates the Events then sends them to Splunk periodically.
    """

    def __init__(self, host: str = '', port: int = '8088', token: str = '',
                 function_name: str = 'vpcFlowLogsProcessor'):
        if not host or not token:
            raise ValueError('Host and Token cannot be empty!')

        splunk = _MySplunkHandler(
            host=host,
            hostname='serverless',
            port=port,
            token=token,
            sourcetype='aws:cloudwatchlogs:vpcflow',
            source=f'lambda:{function_name}',
            timeout=30,
            flush_interval=7.5,
            index='main'
        )

        logger.addHandler(splunk)

    # Send logs to Splunk in the JSON format it understands
    def forward(self, event):
        logger.info(event)

    def finalize(self):
        force_flush()


class DynamoDBLogForwarder(LogForwarder):
    """
    This Forwarder accumulates the Events then sends them to DynamoDB in batches of 25.
    """

    def __init__(self, region: str = 'us-east-1', table: str = 'VPCLogQueue', endpoint_url: str =None):
        self.dynamodb = boto3.resource('dynamodb', region_name=region, endpoint_url=endpoint_url)
        self.table = self.dynamodb.Table(table)
        self.__events = []

    def forward(self, event):
        self.__events.append({
            'timestamp': int(event['time'].split('.')[0]),
            'message': event['event']
        })
        # batches fail if send more than 25 put requests in one go
        if len(self.__events) == 25:
            self.finalize()

    def finalize(self):
        with self.table.batch_writer() as writer:
            for event in self.__events:
                writer.put_item(Item=event)
